potencia(0,1).
potencia(B,1,B).
potencia(B,N,R):-
	B > 0,
	N >= 0,
	N1 is N-1,
	potencia(B,N1,R1),
	R is B * R1.

