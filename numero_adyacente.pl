numeros([[u,n,o],[d,o,s],[t,r,e,s],[c,u,a,t,r,o],[c,i,n,c,o],[s,e,i,s],[s,i,e,t,e],[o,c,h,o],[n,u,e,v,e]]).

%quitaRepes([],[]) - Es el SORT
%Interseccion(N,U,X) - INTERSECTION

compara([_]).
%compara([X|[]]).
compara([N,U|Meros]):-
	sort(N,N1),
	sort(U,U1),
	intersection(N1,U1,[_]),
	compara([U|Meros]).


imprime([]).
imprime([H|T]):-
	write(H),
	nl,
	imprime(T).

%Para poner un predicado sin parametros se quitan los parentesis
ordenar:-
	numeros(X),
	permutation(X,Y),
	compara(Y),
	imprime(Y).

%Lista original y lo vamos a ir cambiando de lugar read(X) para leer en teclado
