% **Juego de loteria**
% Para generar numeros aleatorios: random(min,max,valor).
% * Definir una lista con 52 figuras diferentes.
%figuras(lista,['gallo','diablito','dama','catrin','paraguas','sirena','escalera','botella','barril','arbol',
%        'melon','valiente','gorrito','muerte','pera','bandera','bandolon','violoncello','garza','pajaro',
%        'mano','bota','luna','cotorro','borracho','negrito','corazon','sandia','tambor','camaron',
%        'jaras','musico','arana','soldado','estrella','cazo','mundo','apache','nopal','alacran',
%        'rosa','calavera','campana','cantarito','venado','sol','corona','chalupa','pino','pescado',
%        'palma','maceta']). %,'arpa','rana'

figuras(lista,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                21,22,23,24,25,26,27,28,28,30,31,32,33,34,35,36,37,38,39,40,
                41,42,43,44,45,46,47,48,49,50,51,52]).

carta(c1,[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]).

muestraLista([]).
muestraLista([X|L]):-
	write(X),
	nl,
	muestraLista(L).

muestraMatriz([]).
muestraMatriz([A|B]):-
    muestraLista(A),
    muestraMatriz(B).

buscarElemento(X,[X|_]).
buscarElemento(X,[_|Yx]):-
  buscarElemento(X,Yx).

elimina(X,[X|L],L).
elimina(_,[],[]).
elimina(X,[H|T],[H|T1]):-
	elimina(X,T,T1).

longitud([],0).
longitud([_|L],R):-
	longitud(L,R1),
	R is R1+1.

%V = Elemento a buscar y Y2 = Lista
%repetido(V,[_|Y2]):-
%  not(buscarElemento(V,Y2)).
%repetido(V,[_|Y2],R):-(not(buscarElemento(V,Y2)),R =:= V + 0,write("No existe"),nl).

repetido(V,[_|Y2],R):-
  (not(buscarElemento(V,Y2)),R is V + 0,write("No existe"),nl);!,
  (generaAleatorioLista(R),write("Existe, aleatorio: "),nl,write(V),nl,write(R),nl,repetido(V,Y2,R)).

generaAleatorioLista(V):-
  figuras(lista,Lista),
  %write(Lista),
  longitud(Lista,Lon),
  random(1,Lon,V).

eliminar(Elemento,L):-
  figuras(L,Lista),
  elimina(Elemento,Lista,Resultado),
  muestraLista(Resultado).

generarCartaLista([],[]).
generarCartaLista([X|Y],[X2|Y2]):-
  generaAleatorioLista(R),
  write(R),nl,
  muestraLista(Y),
  muestraLista(Y2),
  repetido(R,Y2,V),

  %nl,write(V),write(Y),
  %not(buscarElemento(V,Y2)),
  %buscarElemento(V,Y2),
  %nl,write(X),write(V),
  X2 is X + V,
  generarCartaLista(Y,Y2).

generarCartaMatriz([],[]).
generarCartaMatriz([H|T],[H1|T1]):-
  generarCartaLista(H,H1),
  generarCartaMatriz(T,T1).




muestra(L):-
  carta(L,Lista),
  muestraMatriz(Lista).

genera:-
  carta(c1,Lista1),
  generarCartaMatriz(Lista1,R1),
  carta(c1,Lista2),
  generarCartaMatriz(Lista2,R2),
  muestraLista(R1),nl,
  muestraLista(R2).

% * Generar 2 cartas de 4x4 (16 figuras).
%   Los numeros de figuras validos van del 1 - 52.
%   No deben de repetirse las figuras generadas.

% * Al estar presentando las cartas, se deben de quitar del maso de las 52 figuras.
% * Cada vez que se menciona una carta, se tiene que validar que la figura
%   se encuentre en una de las cartas de los jugadores. Si la figura se encuentra en la carta,
%   se debe de poner un 0 (poner ficha en fisico) para saber que esa figura ya salio.
% * Cada vez que se pone un 0 en alguna figura de la carta, se debe de verificar si se hizo:
%   centro, cuatro esquinas, chorro o buenas.
% * CENTRO: las 4 figuras del centro de la carta.
% * CUATRO ESQUINAS: las 4 figuras de las esquinas de la carta.
% * CHORRO: 4 figuras concecutivas dentro de la carta.
% * BUENAS: Todas las fuguras dentro de la carta.
% * El juego va a terminar cuando se hagan buenas (cuando todas las figuras de una carta tengan 0). Verificar
%   que no existan PEDRADAS.
% * En caso de que los jugadores hagan PEDRADA es decir, que alguno gane una jugada al mismo tiempo,
%   se repetira el juego, y el primero que le salga una figura de su carta gana la PEDRADA.
