herbivoro(vaca).
herbivoro(oveja).
carnivoro(leon).
persona(hugo).
persona(paco).
persona(luis).

objeto(desodorante).
objeto(lapiz).

animal(X):- herbivoro(X);carnivoro(X).

% 1) Las algas usan medias rojas
usanMediasRojas(algas).

% 2) Toda persona o objeto o animal que usa deshodorante sabe tocar el saxo
tocaSaxo(X):- usaDesodorante(X).
tocaSaxo(X):- not(usanMediasRojas(X)).

% 3) Todo lo que eche humo usa desodorante
echaHumo(X):- persona(X).
usaDesodorante(X):- echaHumo(X).

% 4) Nada ni nadie que use medias rojas puede usar el saxo

% 5) Todas las personas pueden usar un objeto
usaObjeto(X):- persona(X).

% 6) Si una persona usa desodorante no hara la tarea
haceTarea(X):- not(usaDesodorante(X)).