%Agregar un elemento al principio de una lista
agregarP(X,[],[X]).
agregarP(X,L,[X|L]).

%Agregar un elemento al final de una lista
agregarF(X,[],[X]).
agregarF(X,[H|T],[H|R]):-
    agregarF(X,T,R).

%Regresar el elemento N de una lista
elemento(_,[H|_],H,_).
elemento(X,[H|L],_,Ac):-
    X =:= Ac, %Necesito igualar esos dos valores
    elemento(X,L,H,Ac).
elemento(X,[_|L],R,Ac):-
    Acc is Ac + 1,v %Si es diferente, sumar los valores
    elemento(X,L,R,Acc).