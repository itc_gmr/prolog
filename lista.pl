%Mostrar datos a una lista
lista([a,b,c,d,e,f]).
muestraLista([]).
muestraLista([X|L]):-
	write(X),
	nl,
	muestraLista(L).

%Elimina un dato de una lista
%Elimina(elemento a eliminar,listaVieja,nuevaLista)
elimina([X,[X|L],L]).
elimina(X,[],[]).
elimina(X,[H|T],[H|T1]):-
	elimina(X,T,T1).

%Agregar dato de una lista
agrega(X,[],[X]).
agrega(X,L,[X|L]).

%Tama�o de una lista ?longitud([a,b,c],3).
longitud([],0).
longitud([X|L],R):-
	longitud(L,R1),
	R is R1+1.
