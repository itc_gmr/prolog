jugador(j1).
jugador(j2).

empieza:-write("Cuantos palitos? \n"),read(Palitos),
	write("Quien empieza? \n"), read(Quien), jugador(Quien),
	nim(Palitos,Quien).

nim(1,Q):-write(Q), write(" ya perdiste!! :( \n").
nim(P,Q):-P<1, write(Q), write(" ya ganaste!! :) \n").
nim(P,Q):-
	write(" jugador: "), write(Q),
	write(" Cuantos quitamos ? \n"),
	read(C), % C > 0, C < 4,
	valida123(C, C1),
	P1 is P - C1,
	write(" quedan: "), write(P1), nl,
	jugador(Q1), Q1 \= Q,
	nim(P1,Q1).

%Adecuar para que valide limite superior e inferior
valida123(C,C):-C > 0, C < 4.
valida123(C,R):-
	(C < 1; C > 3),
	write(" Validos 1, 2 y 3... teclee otro numero "),
	read(C1),
	valida123(C1,R).

