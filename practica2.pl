planta(alga).
objeto(palo).
objeto(desodorante).
objeto(saxo).
objeto(medias).
animal(perro).
animal(gato).
persona(pedro).
persona(juan).
hippie(juan).

feliz(X):-hippie(X),persona(X).
usa(X,Y):-planta(X),objeto(Y).
usa(X,Y):-(planta(X);objeto(X);persona(X)),objeto(Y).
toca(X,Y):-usa(X,desodorante).
