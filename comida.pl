herbivoro(vaca).
herbivoro(oveja).
carnivoro(leon).
hortaliza(tomate).
hortaliza(zanahoria).
fruta(manzana).
pescado(besugo).
carne(salchicha).
fideos(spaguetti).

%Creamos las reglas
comer(X,Y):-
    (carnivoro(X),(herbivoro(Y);pescado(Y);carne(Y)));
    (herbivoro(X),(hortaliza(Y);fruta(Y))).
