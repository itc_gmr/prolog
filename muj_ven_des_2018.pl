%EJERCICIO 1:
%====================
%Sobre líquidos venenosos

%El Sr. Ido, el químico, tiene seis frascos llenos de líquidos coloreados. 
%Hay uno de cada color: rojo, anaranjado, amarillo, verde, azul y violeta. 
%El señor Ido sabe que algunos de esos líquidos son tóxicos, pero no recuerda cuales...
%Sin embargo, sí recuerda algunos datos. 
%En cada uno de los siguientes pares de frascos hay uno con veneno y otro no:

%a) los frascos violeta y azul
%b) los frascos rojo y amarillo
%c) los frascos azul y anaranjado
unoSiunoNo(violeta,azul).
unoSiunoNo(rojo,amarillo).
unoSiunoNo(azul,anaranjado).

venenoso(X):- (unoSiunoNo(X,Y);unoSiunoNo(Y,X)), noVeneno(Y).
%El Sr. Ido recuerda también que en estos otros pares de frascos hay uno sin veneno:

%d) el violeta y el amarillo
%e) el rojo y el anaranjado
%f) el verde y el azul
unoSin(violeta,amarillo).
unoSin(rojo,anaranjado).
unoSin(verde,azul).

noVeneno(rojo).
noVeneno(X):- (unoSin(X,Y);unoSin(Y,X)), venenoso(Y).

%¡Ah! Casi lo olvido, añade el Sr. Ido, el líquido del frasco rojo no es venenoso. 
%¿Qué frascos tienen veneno?

%EJERCICIO 2:
%======================================
%Encuentra la ocupación de cada mujer:
mujer(luisa).
mujer(clara).
mujer(maria).
mujer(nelida).
ocupacion(directora_de_orquesta).
ocupacion(jardinera).
ocupacion(florista).
ocupacion(disenadora).
%(a) Clara es violentamente alérgica a las plantas.
alergica(clara,plantas).
%(b) Luisa y la florista comparten el departamento
noEs(luisa,florista).
%(c) A María y Luisa les gusta solamente la música rock
noEs(maria,directora_de_orquesta).
noEs(luisa,directora_de_orquesta).
%(d) La jardinera, la diseñadora de modas y Nélida no se conocen entre sí.
noEs(nelida,jardinera).
noEs(nelida,disenadora).
%e) una mujer no puede tener una ocupación que esté relacionada con algo a lo que es alérgica:
noEs(clara,jardinera).
noEs(clara,florista).
%f) cada mujer tiene un solo trabajo, y  cada trabajo es ocupado por una sola mujer 
%las cuatro mujeres elegidas y las cuatro ocupaciones  deben ser diferentes entre sí.
diferentes(A,B,C,D):- A\=B, A\=C, A\=D, B\=C, B\=D, C\=D.

respuesta([M1,O1],[M2,O2],[M3,O3],[M4,O4]):- 
    mujer(M1), mujer(M2), mujer(M3), mujer(M4),
    ocupacion(O1), ocupacion(O2), ocupacion(O3), ocupacion(O4),
    diferentes(M1,M2,M3,M4),diferentes(O1,O2,O3,O4),
    not(noEs(M1,O1)),not(noEs(M2,O2)),not(noEs(M3,O3)),not(noEs(M4,O4)).

/*  	?- respuesta(X).
	X=[[clara,diseñadora],[luisa,jardinera],
	   [maria,florista],[nelida,directora_de_orquesta]]
	Yes
*/