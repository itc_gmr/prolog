
%La lista A y B deben de ser iguales
a2b([],[]).
a2b([a|L1],[b|L2]):- a2b(L1,L2).

addThreeAndDouble(X,Y):-
    Y is (X+3)*2.

%Número mayor
may([],A,A).
may([H|T],OldA,R):-
    H > OldA,
    may(T,H,R).
may([H|T],OldA,R):-
    OldA >= H,
    may(T,OldA,R).

mayor([H|L],R):-
    may(L,H,R).

%Longitud usando acumuladores
longitud([],OldA,OldA).

longitud([_|T],R,OldA):-
    NewA is OldA + 1,
    longitud(T,R,NewA).

lon(Lis,Lon):-
    longitud(Lis,Lon,0).

%Longitud sin acumuladores

len([],0).
len([_|T],R):-
    len(T,X),
    R is X + 1.
