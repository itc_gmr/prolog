%  Base de conocimiento:   Diccionario espanol-frances
trad('ciudad,' , 'ville,').
trad('conocer' , 'connatre').
trad('las' , 'les').
trad('No me gustan' , 'Je naime pas').
trad('vacaciones' , 'vacances').
trad('Antes de' , 'Avant de').
trad('acento' , 'accent').
trad('me' , 'il me').
trad('yo' , 'je').
trad('prefiero' , 'prfre').
trad('visitar' , 'visite').
trad('relajantes,' , 'dtentes,').
trad('lugares' , 'lieux').
trad('historicos' , 'historiques').
trad('y' , 'et').
trad('parece' , 'semble').
trad('idioma' , 'langue').
trad('simple' , 'simple').
trad('de' , 'a').
trad('calles' , 'rues').
trad('pequenas.' , 'petits.').
trad('por' , 'pour').
trad('una' , 'une').
trad('estaba' , 'jtais').
trad('interesada' , 'intress').
trad('aprender' , 'apprendre').
trad('italiano.' , 'litalien.').
trad('Porque' , 'Parce que').
trad('me gusta' , 'jaime').
trad('mucho' , 'beaucoup').
trad('su' , 'son').
trad('comenzar' , 'commencer').
trad('estudiar' , 'tudier').
trad('el' , 'le').
trad('frances,' , 'franais').
trad('a' , 'a').
trad('un' , 'une').
trad('caminar' , 'marcher').

% Base de conocimiento:   Cuentos
cuentos(uno, ['No me gustan', 'las', 'vacaciones', 'relajantes,' ,'yo','prefiero',
	'visitar','una','ciudad,' , 'conocer','lugares','historicos','y','caminar','por',
	'las','calles','pequenas.' ]).

cuentos(dos, ['Antes de', 'comenzar', 'a', 'estudiar', 'frances,', 'estaba',
	'interesada', 'por', 'aprender', 'italiano.', 'Porque', 'me gusta', 'mucho',
	'su', 'acento', 'y', 'me', 'parece', 'un', 'idioma', 'simple', 'de', 'aprender' ]).

%traduceCuento(uno).
traduce([],[]).
traduce([X|L],[Y|L2]):-
	trad(X,Y),
    write(Y),
    tab(1),
	traduce(L,L2).

traduceCuento(Cuento):-
	cuentos(Cuento,Lista),
	traduce(Lista,_).