% **Juego de domino decimal**
% *Lista de listas, en ella se enlistan las piezas y cada pieza tiene
% una lista de dos elementos

% *Repartir fichas de manera aleatorio 1 hasta la longitud de la lista
% (7 fichas), el predicado reparte se define con un parametro con el
% numero de fichas a jalar
% *No deben de repetirse las fichas
% *Se tiene que sacar la
% ficha de la lista y asignarlas a los jugadores

% *Ver si alguien tiene la mula del 5, si se tiene la mula del 5 el que
% la puso gana los 10 puntos,si nunguno tiene la mula, se busca una
% ficha con multiplo de 5, se le van sumando los puntos

% *Una vez repartidas, se van poniendo las fichas, si las esquinas son
% multiplos de 5, se suman los puntos del jugador que realizo un
% multiplo de 5 (hasta que alguien ya no tenga fichas disponibles)

% *Cuando se tengan fichas disponibles,se agarra una sola ficha del
% metodo de repartir, se valida si al jugador le sirve la ficha

% *El juego termina cuando se le acaban las fichas a alguno de los
% jugadores, al jugador que le quederan fichas se le suman las fichas y
% el total se redondea al multiplo de 5 mas cercano y se le suman a los
% puntos de la persona que se le acabaron las fichas Gana el jugador que
% tenga mas puntos

% *Jugar hasta N cantidad de puntos
