mujer(clara).
mujer(luisa).
mujer(maria).
mujer(nelida).
ocupacion(disenadora).
ocupacion(jardinera).
ocupacion(florista).
ocupacion(dirOrquesta).

%(a) Clara es violentamente al�rgica a las plantas.
%alergica(clara,plantas).
noEs(clara,florista).
noEs(clara,jardinera).

%(b) Luisa y la florista comparten el departamento
noEs(luisa,florista).

%(c) A Mar�a y Luisa les gusta solamente la m�sica rock
noEs(maria,dirOrquesta).
noEs(luisa,dirOrquesta).

% (d) La jardinera, la dise�adora de modas y N�lida no se conocen ents�.
noEs(nelida,jardinera).
noEs(nelida,disenadora).

/*f) cada mujer tiene un solo trabajo, y  cada trabajo es ocupado por una sola mujer las cuatro mujeres elegidas y las cuatro ocupaciones  deben ser diferentes entre s�.*/
diferentes(A,B,C,D):-A\=B,A\=C,A\=D,B\=C,B\=D,C\=D.

respuesta([[Mu1,Oc1],[Mu2,Oc2],[Mu3,Oc3],[Mu4,Oc4]]):-
	mujer(Mu1),mujer(Mu2),mujer(Mu3),mujer(Mu4),
	ocupacion(Oc1),	ocupacion(Oc2),	ocupacion(Oc3),	ocupacion(Oc4),
	diferentes(Mu1,Mu2,Mu3,Mu4),
	diferentes(Oc1,Oc2,Oc3,Oc4),
	not(noEs(Mu1,Oc1)),
	not(noEs(Mu2,Oc2)),
	not(noEs(Mu3,Oc3)),
	not(noEs(Mu4,Oc4)).

