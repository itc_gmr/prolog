%  Base de conocimiento:   Diccionario espanol-frances
trad('ciudad,' , 'ville,').
trad('conocer' , 'conna�tre').
trad('las' , 'les').
trad('No me gustan' , 'Je n�aime pas').
trad('vacaciones' , 'vacances').
trad('Antes de' , 'Avant de').
trad('acento' , 'accent').
trad('me' , 'il me').
trad('yo' , 'je').
trad('prefiero' , 'pr�f�re').
trad('visitar' , 'visite').
trad('relajantes,' , 'd�tentes,').
trad('lugares' , 'lieux').
trad('historicos' , 'historiques').
trad('y' , 'et').
trad('caminar' , 'marcher').
trad('parece' , 'semble').
trad('idioma' , 'langue').
trad('simple' , 'simple').
trad('de' , '�').
trad('calles' , 'rues').
trad('pequenas.' , 'petits.').
trad('por' , 'pour').
trad('una' , 'une').
trad('estaba' , 'j��tais').
trad('interesada' , 'int�ress�').
trad('aprender' , 'apprendre').
trad('italiano.' , 'l�italien.').
trad('Porque' , 'Parce que').
trad('me gusta' , 'j�aime').
trad('mucho' , 'beaucoup').
trad('su' , 'son').
trad('comenzar' , 'commencer').
trad('estudiar' , '�tudier').
trad('el' , 'le').
trad('frances,' , 'fran�ais').
trad('a' , '�').
trad('un' , 'une').

% Base de conocimiento:   Cuentos
cuentos(uno, ['No me gustan', 'las', 'vacaciones', 'relajantes,' ,'yo','prefiero',
	'visitar','una','ciudad,' , 'conocer','lugares','historicos','y','caminar','por',
	'las','calles','pequenas.' ]).

cuentos(dos, ['Antes de', 'comenzar', 'a', 'estudiar', 'frances,', 'estaba',
	'interesada', 'por', 'aprender', 'italiano.', 'Porque', 'me gusta', 'mucho',
	'su', 'acento', 'y', 'me', 'parece', 'un', 'idioma', 'simple', 'de', 'aprender' ]).

%traduceCuento(uno).
muestraLista([]):-nl.
muestraLista([X|L]):-
	write(X),
	tab(1),
	muestraLista(L).

traduce([],[]).
traduce([X|L],[Y|L2]):-
	trad(X,Y),
	traduce(L,L2).

traduceCuento(Tale):-
	cuentos(Tale,Lista),
	write('Original:'),nl,
	muestraLista(Lista),
	traduce(Lista,ListaTrad),
	write('Traduccion:'),nl,
	muestraLista(ListaTrad).
