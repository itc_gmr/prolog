dentro(irina,natasha).
dentro(natasha,olga).
dentro(olga,katarina).

%Caso base
adentro(X,Y):-dentro(X,Y).
%Caso recursivo
adentro(X,Y):-dentro(X,Z),adentro(Z,Y).
