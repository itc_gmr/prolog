
%a) los frascos violeta y azul
%b) los frascos rojo y amarillo
%c) los frascos azul y anaranjado

unoSiOtroNo(violeta,azul).
unoSiOtroNo(rojo,amarillo).
unoSiOtroNo(azul,anaranjado).

venenoso(X):-(unoSiOtroNo(X,Y);unoSiOtroNo(Y,X)),noVenenoso(Y).

%d) el violeta y el amarillo
%e) el rojo y el anaranjado
%f) el verde y el azul

unoSin(violeta,amarillo).
unoSin(rojo,anaranjado).
unoSin(verde,azul).

noVenenoso(rojo).
noVenenoso(X):-(unoSin(X,Y);unoSin(Y,X)),venenoso(Y).
